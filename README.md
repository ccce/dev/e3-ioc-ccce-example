# e3-ioc-ccce-example

This is an example IOC for deployment with the CCCE deployment tool.

## Configuration

As an integrator you of course need to customise the startup script `st.cmd` to run your IOC.
You should also configure `ioc.yml`. This is where you specify
* Type of IOC (conda/nfs)
* Additional Packages
* Version of base/require
* Realtime/not realtime kernel
* Additional [cell modules](https://confluence.esss.lu.se/display/E3/Cellmode+for+e3+users) to install
* Additional PVA/CA IP addresses for connecting to PVs on different subnets

It is possible that some of this will very as need arises over time.
